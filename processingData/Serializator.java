package processingData;

import java.io.*;


//Hay que implementar la serializacion en los beans

public class Serializator{

	//hacemos un serializador y deserializador con Generics
	
	
	public static <T> void serializeFile(T item, String nombreArchivo){
		
		/*
		 * metemos en el try el FileReader y el BufferedReader 
		 * para que se encargue el try de gestionar el cerrado de datos y de buffer
		*/
		try (FileOutputStream fos = new FileOutputStream(nombreArchivo); ObjectOutputStream oos = new ObjectOutputStream(fos)){
			
			
			oos.writeObject(item);	//serializamos el item
			
			
		} catch (IOException e) {
			System.out.println("Mensaje: " + e.getMessage() + "\nTrace: " + e.getStackTrace());
		}
		
	}
	
	
	public static <T> void deSerializeFile(T item, String ruta){
		
		try (FileInputStream fos = new FileInputStream(ruta); ObjectInputStream oos = new ObjectInputStream(fos))
		{
			@SuppressWarnings("unchecked")
			T oc = (T) oos.readObject();
			System.out.println(oc);
			
			
		} catch (IOException e) {
			System.out.println("Mensaje: " + e.getMessage() + "\nTrace: " + e.getStackTrace());
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
	}
	
}
